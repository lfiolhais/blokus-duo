#include <stdio.h>

#include "Cells.h"

// Cell Functions

// Print cell to stdout
void print_cell(Cells *self) {
  if (self->has_piece == TRUE) {
    if (self->player == PlayerOne) {
      printf(" P1 ");
    } else {
      printf(" P2 ");
    }
  } else {
    printf(" 00 ");
  }
  return;
}

// Set set cell value to a given value
//
// # Arguments
// * self -> cell to update
// * value -> new value of cell
void set_piece(Cells *self, Bool value) {
  self->has_piece = value;
  return;
}

// Check if there is a piece set in a cell
//
// # Arguments
// * self -> cell to check
//
// # Return Value
// True if there is a piece already set in the cell, false otherwise
Bool has_piece(Cells *self) {
  return self->has_piece;
}

// Return the player which set the piece
//
// # Arguments
// * self -> cell to check
//
// # Return Value
// The player
Player get_player_piece(Cells *self) {
  return self->player;
}

// Set set cell player to a player
//
// # Arguments
// * self -> cell to update
// * player -> new player
void set_cell_player(Cells *self, Player player) {
  self->player = player;
  return;
}
