#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>

#include "misc.h"

// Convert a character to an unsigned 8 bit number
//
// # Arguments
// * char_2_convert -> character to convert to number
//
// # Return Values
// Converted character
uint_8 char_2_int(char char_2_convert) {
  // Inside interval 1-9
  if (char_2_convert >= '1' && char_2_convert <= '9') {
    return (uint_8) char_2_convert - '1'; // vectors addressed from 1 to N, instead from 0 to N-1
  } else {
    // Inside interval a-e
    return (uint_8) char_2_convert - 'b' + 10;
  }
}

// Convert a number to a char
//
// # Arguments
// * int_2_convert -> number to convert to char
//
// # Return Values
// Converted number
char int_2_char(uint_8 int_2_convert) {
  // Inside interval 1-9
  if (int_2_convert < 10) {
    return (char) int_2_convert + '1'; // vectors addressed from 1 to N, instead from 0 to N-1
  } else {
    // Inside interval a-e
    return (char) int_2_convert - 10 + 'a';
  }
}

// Prints the formatted string to stderr
//
// # Arguments
// * format => format of the string to print
// * args => arguments to print
void printerr(const char *restrict format, ...) {
  va_list ap;

  va_start(ap, format);

  char previous_char = '\0';
  while (*format) {
    if (previous_char == '%') {
      switch (*format) {
      // Print String
      case 's': {
        char *s = va_arg(ap, char *);
        fprintf(stderr, "%s", s);
        break;
      }
      // Print int
      case 'd': {
        int d = va_arg(ap, int);
        fprintf(stderr, "%d", d);
        break;
      }
      // Print char
      case 'c': {
        char c = (char)va_arg(ap, int);
        fprintf(stderr, "%c", c);
        break;
      }
      case 'u': {
        unsigned u = va_arg(ap, unsigned);
        fprintf(stderr, "%u", u);
        break;
      }
      // Special Case: print % when the previous character is % and the
      // current character is %
      default:
        fprintf(stderr, "%c", *format);
      }
    } else if ((char)(*format) != '%') {
      // Print non-special character
      fprintf(stderr, "%c", *format);
    }
    previous_char = *format;
    format++;
  }

  va_end(ap);

  return;
}
