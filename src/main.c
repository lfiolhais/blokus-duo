#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <strings.h>

#include "Board.h"
#include "Pieces.h"
#include "Players.h"
#include "Tree.h"
#include "ref_fsm.h"

int main(void) {
  Ref_States state = 0, state_nxt = 0;
  char param = '\0';
  char op_play[4];

  // Create board
  Board *board = new_board();
  // Create players
  Players *player_one;
  Players *player_two;

  Bool no_more_plays = FALSE;

  Bool first_play = TRUE;
  char x_coord = '\0';
  char y_coord = '\0';
  char piece_code = '\0';
  char rotation_code = '\0';
  ValidateMoveResult validate_result;
  validate_result.n_contact_edges_detected = 0;
  Players *player = NULL;
  Players *opponent = NULL;
  Node *best_node = NULL;

  //////////////////////////////////////////////////////////////////////////////
  // Main game loop:
  // 1 - Ask for user input
  // 1.1 - Validate user input
  // 2 - Perform move
  // 3 - Update playbooks
  // 4 - Update scores
  // 5 - Change player
  //////////////////////////////////////////////////////////////////////////////
  while (1) {
    board = new_board();
    player_one = new_player(PlayerOne);
    player_two = new_player(PlayerTwo);
#ifdef DEBUG
    print_board(board);
#endif
    state = START;
    while (state != END) {
#ifdef DEBUG
      printf("Current state is %d\n", state);
      printf("Next state is %d\n", state_nxt);
#endif
      get_next_state(op_play, &param, &state, &state_nxt, no_more_plays,
                     &player, &opponent, player_one, player_two, &first_play);
#ifdef DEBUG
      printf("New state is %d\n", state);
      printf("New Next state is %d\n", state_nxt);
#endif
      if (state == FIRST || state == FIRST_US || state == US || state == THEM) {
        if (op_play[2] != '0') {
          // Store local variables
          x_coord = op_play[1];
          y_coord = op_play[0];
          piece_code = op_play[2];
          rotation_code = op_play[3];

          // Switch player pointers
          if (player->player == PlayerOne) {
            player = player_two;
            opponent = player_one;
          } else {
            player = player_one;
            opponent = player_two;
          }

          // Validate the input
          validate_result = validate_move(board, first_play, player, piece_code,
                                          x_coord, y_coord, rotation_code);
        } else {
#ifdef DEBUG
          printf("Player %d: ", player->player);
#endif
          best_node = get_best_move(first_play, player, board, param, opponent);

          if (best_node == NULL) {
#ifdef DEBUG
            printf("No plays generated, exiting.\n");
#endif
            no_more_plays = TRUE;
	    continue;
          }
#ifdef DEBUG
          printf("Score is %d\n", best_node->score);

          printf("Play is %c %c %c %c \n", best_node->x, best_node->y,
                 best_node->piece, best_node->rotation);
#else
          printf("%c%c%c%c", best_node->x, best_node->y,
                           best_node->piece, best_node->rotation);
#endif
          // Store local variables
          x_coord = best_node->y;
          y_coord = best_node->x;
          piece_code = best_node->piece;
          rotation_code = best_node->rotation;
        }

        // Apply input
        play_piece(board, piece_code, x_coord, y_coord, player->player,
                   rotation_code);
        // Add possible plays to the player's playbook
        player_update_edges(player, opponent, piece_code, rotation_code,
                            x_coord, y_coord,
                            validate_result.edge_contact_coord,
                            validate_result.n_contact_edges_detected, board);
        // Update player's piece data
        player_update_pieces(player, piece_code);

        // Update score
        player_update_score(player, piece_code);
#ifdef DEBUG
        print_board(board);
#endif
        // switch player
        if (op_play[2] != '0' && player->player == PlayerOne) {
          player = player_two;
          opponent = player_one;
        } else if (op_play[2] != '0') {
          player = player_one;
          opponent = player_two;
        }

#ifdef DEBUG
        // Print players future moves
        print_player(player_one);
        print_player(player_two);
#endif
      }

      state = state_nxt;
    }

    //////////////////////////////////////////////////////////////////////////////
    // Free memory
    //////////////////////////////////////////////////////////////////////////////
    if (board != NULL) {
      free(board);
    }

    if (player_one != NULL) {
      if (player_one->playbook != NULL) {
        for (size_t i = 0; i < player_one->playbook->size; i++) {
          if (player_one->playbook->data[i] != NULL) {
            free(player_one->playbook->data[i]);
          }
        }
        if (player_one->playbook->data != NULL) {
          free(player_one->playbook->data);
        }
        free(player_one->playbook);
      }
      free(player_one);
    }

    if (player_two != NULL) {
      if (player_two->playbook != NULL) {
        for (size_t i = 0; i < player_two->playbook->size; i++) {
          if (player_two->playbook->data[i] != NULL) {
            free(player_two->playbook->data[i]);
          }
        }
        if (player_two->playbook->data != NULL) {
          free(player_two->playbook->data);
        }
        free(player_two->playbook);
      }
      free(player_two);
    }
  }

  exit(0);
}
