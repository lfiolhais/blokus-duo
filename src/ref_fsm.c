// Referee Finite State Machine
#include "ref_fsm.h"

#include <stdio.h>
#include <strings.h>
#include <unistd.h>

static const char ID[4] = "1P2\0";
static const char PASS[5] = "0000\0";

void get_next_state(char *op_play, char *param, Ref_States *state,
                    Ref_States *state_nxt, Bool no_more_plays, Players **player,
                    Players **opponent, Players *player_one,
                    Players *player_two, Bool *first_play) {
  char buffer[7];
  char cmd = '\0';
  int n;

  if (*state == THEM || *state == START || *state == FIRST) {
#ifdef DEBUG
    printf("Referee: ");
#endif
    // Read Input
    fflush(0);

    if (*state == THEM) {
      n = read(0, buffer, 5);
    } else if (*state == START) {
      n = read(0, buffer, 1);
      cmd = buffer[0];
    } else if (*state == FIRST) {
      n = read(0, buffer, 1);
      if (buffer[0] == '2') {
        n = read(0, buffer, 1);
        *param = buffer[0];
      } else {
        n = read(0, buffer, 5);
        *param = buffer[0];
      }
    }

    buffer[n] = '\0';

    // State machine
    if (cmd == '9') {
      *state = END;
#ifdef DEBUG
      printf("END\n");
#endif
    }
  }

  switch (*state) {
  case START:
    if (cmd == '0') {
#ifdef DEBUG
      printf("%s\n", ID); // write to RS-232
#else
      printf("%s", ID); // write to RS-232
#endif
      *state_nxt = FIRST;
#ifdef DEBUG
      printf("FIRST\n");
#endif
    }
    break;
  case FIRST:
#ifdef DEBUG
    printf("param = %c\n", *param);
#endif
    // Check if we are the first player, or the second
    if (cmd == '2') {
      *player = player_one;
      *opponent = player_two;

      op_play[0] = '0';
      op_play[1] = '0';
      op_play[2] = '0';
      op_play[3] = '0';
      *state_nxt = THEM;
    } else { // cmd = '3'
      *player = player_two;
      *opponent = player_one;

      op_play[0] = buffer[1];
      op_play[1] = buffer[2];
      op_play[2] = buffer[3];
      op_play[3] = buffer[4];
      *state_nxt = FIRST_US;
    }

    *first_play = TRUE;
#ifdef DEBUG
    printf("NEXT\n");
#endif
    break;
  case FIRST_US:
    op_play[0] = '0';
    op_play[1] = '0';
    op_play[2] = '0';
    op_play[3] = '0';

    *state_nxt = THEM;
    *first_play = TRUE;
    break;
  case FIRST_THEM:
    op_play[0] = buffer[2];
    op_play[1] = buffer[3];
    op_play[2] = buffer[4];
    op_play[3] = buffer[5];

    *state_nxt = US;
    *first_play = TRUE;
    break;
  case THEM:
    op_play[0] = buffer[1];
    op_play[1] = buffer[2];
    op_play[2] = buffer[3];
    op_play[3] = buffer[4];

    *first_play = FALSE;
    *state_nxt = US;

    if (op_play[2] == '0') {
      *state = SKIP;
#ifdef DEBUG
      printf("SKIP\n");
#endif
    }
    break;
  case US:
    op_play[0] = '0';
    op_play[1] = '0';
    op_play[2] = '0';
    op_play[3] = '0';

    *first_play = FALSE;
    *state_nxt = THEM;

    if (no_more_plays == TRUE) {
      *state = SKIP;
#ifdef DEBUG
      printf("SKIP\n");
#endif
    }
    break;
  case SKIP:
#ifdef DEBUG
    printf("%s\n", PASS); // write to RS-232
#else
    printf("%s", PASS); // write to RS-232
#endif
    break;
  default: // END
    *state_nxt = END;
  }
}
