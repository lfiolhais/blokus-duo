#include <stdio.h>
#include <stdlib.h>

#include "Board.h"

// Prototypes
static Bool is_first_move_correct(uint_8 x, uint_8 y, Piece piece);
static Bool check_overlap(Board *self, uint_8 x, uint_8 y, Piece piece);
static Bool check_edge_contact(Players *player, uint_8 x, uint_8 y,
                               Piece *piece, ValidateMoveResult *result);
static Bool check_side_contact(Board *self, uint_8 x, uint_8 y, Piece *piece,
                               Players *player);
static Bool check_boundaries(uint_8 x, uint_8 y, Piece piece);

// Create Board
Board *new_board(void) {
  Board *board = (Board *)malloc(sizeof(Board));

  if (board == NULL) {
    printerr("Failed to allocate memory to Board. Exiting.\n");
    exit(1);
  }

  for (size_t i = 0; i < 14; i++) {
    for (size_t j = 0; j < 14; j++) {
      set_piece(&board->cells[i][j], FALSE);
    }
  }

  return board;
}

// Set player piece on the board at the specified coordinates and given
// rotation.
//
// # Arguments
// * self -> board to place the piece on
// * piece_char -> character code for the piece to play
// * x_char -> x coordinate in char (rows)
// * y_char -> y coordinate in char (columns)
// * player -> the player performing the play
// * rotation -> character code for the rotation piece
void play_piece(Board *self, char piece_char, char x_char, char y_char,
                Player player, char rotation) {
  // Get piece from dictionary
  Piece piece = get_piece(piece_char, rotation);

  // Convert from char to integer
  uint_8 x = char_2_int(x_char);
  uint_8 y = char_2_int(y_char);

  // Apply piece in x and y coordinates. The for's are required because six
  // pieces will occupy more than one square. In the SW/NW/SE/NE case only one
  // square is used, thus no for's are needed.

  // Apply N parameter
  for (size_t i = 1; i <= piece.N; i++) {
    set_piece(&self->cells[x - i][y], TRUE);
    set_cell_player(&self->cells[x - i][y], player);
  }
  // Apply S parameter
  for (size_t i = 1; i <= piece.S; i++) {
    set_piece(&self->cells[x + i][y], TRUE);
    set_cell_player(&self->cells[x + i][y], player);
  }
  // Apply W parameter
  for (size_t i = 1; i <= piece.W; i++) {
    set_piece(&self->cells[x][y - i], TRUE);
    set_cell_player(&self->cells[x][y - i], player);
  }
  // Apply E parameter
  for (size_t i = 1; i <= piece.E; i++) {
    set_piece(&self->cells[x][y + i], TRUE);
    set_cell_player(&self->cells[x][y + i], player);
  }
  // Apply SW parameter
  set_piece(&self->cells[x + piece.SW][y - piece.SW], TRUE);
  set_cell_player(&self->cells[x + piece.SW][y - piece.SW], player);
  // Apply SE parameter
  set_piece(&self->cells[x + piece.SE][y + piece.SE], TRUE);
  set_cell_player(&self->cells[x + piece.SE][y + piece.SE], player);
  // Apply NW parameter
  set_piece(&self->cells[x - piece.NW][y - piece.NW], TRUE);
  set_cell_player(&self->cells[x - piece.NW][y - piece.NW], player);
  // Apply NE parameter
  set_piece(&self->cells[x - piece.NE][y + piece.NE], TRUE);
  set_cell_player(&self->cells[x - piece.NE][y + piece.NE], player);

  return;
}

// Check if a move is possible
//
// # Arguments
// * self -> board to place the piece on
// * first_play -> is this move the first of the game?
// * player -> the player performing the play
// * piece_char -> character code for the piece to play
// * x_char -> x coordinate in char (rows)
// * y_char -> y coordinate in char (columns)
// * rotation -> character code for the rotation piece
//
// # Return Values
// True if the move is legal, false otherwise
ValidateMoveResult validate_move(Board *self, Bool first_play, Players *player,
                                 char piece_char, char x_char, char y_char,
                                 char rotation) {
  // Create result struct
  ValidateMoveResult result;

  // Get piece from dictionary, already rotated
  Piece piece = get_piece(piece_char, rotation);

  // Convert from char to integer
  uint_8 x = char_2_int(x_char);
  uint_8 y = char_2_int(y_char);

  // Check out of bounds
  if (check_boundaries(y, x, piece) == TRUE) {
    result.tag = ValidateMoveTag_BoundariesError;
    result.ValidateMoveError.BoundariesError[0] = x;
    result.ValidateMoveError.BoundariesError[1] = y;
    return result;
  }

  // Check if player still has that piece available
  uint_32 pieces = player->pieces;
  if (((pieces >> (piece_char - 'a')) & 0x00000001) != 1) {
    result.tag = ValidateMoveTag_UsedPieceError;
    return result;
  }

  // Check first move
  if (first_play == TRUE && is_first_move_correct(x, y, piece) == FALSE) {
    result.tag = ValidateMoveTag_FirstMoveError;
    result.ValidateMoveError.FirstMoveError[0] = x;
    result.ValidateMoveError.FirstMoveError[1] = y;
    return result;
  }

  // Check overlap between different pieces of the same or different players
  if (check_overlap(self, x, y, piece) == TRUE) {
    result.tag = ValidateMoveTag_OverlapError;
    result.ValidateMoveError.OverlapError[0] = x;
    result.ValidateMoveError.OverlapError[1] = y;
    return result;
  }

  // Check for edge to edge contact. This function also updates the result
  // struct so that the occupied edge can be removed from the playbook.
  if (check_edge_contact(player, x, y, &piece, &result) == FALSE) {
    result.tag = ValidateMoveTag_EdgeContactError;
    result.ValidateMoveError.EdgeContactError[0] = x;
    result.ValidateMoveError.EdgeContactError[1] = y;
    return result;
  }

  // Check for side to side contact of the same player
  if (check_side_contact(self, x, y, &piece, player) == TRUE) {
    result.tag = ValidateMoveTag_SideContactError;
    return result;
  }

  // Return edge contact result to remove from the players playbook
  result.tag = ValidateMoveTag_Ok;
  return result;
}

// Check if there is an overlap between the possible moves in the playbook and
// the move that the player requested.
//
// # Arguments
// * player -> the player which contains the playbook
// * x -> the row coordinate
// * y -> the column coordinate
// * piece -> the block to set
//
// # Return Value
// True if the piece contains the coordinate in the playbook, false otherwise
static Bool check_edge_contact(Players *player, uint_8 x, uint_8 y,
                               Piece *piece, ValidateMoveResult *result) {
  uint_16 pb_len = playbook_len(player->playbook);

  result->n_contact_edges_detected = 0;

  if (pb_len == 0) {
    return TRUE;
  }

  for (size_t i = 0; i < pb_len; i++) {
    // Check center
    if (x == player->playbook->data[i][0] &&
        y == player->playbook->data[i][1]) {
      result->edge_contact_coord[result->n_contact_edges_detected][0] =
          (uint_8)(x);
      result->edge_contact_coord[result->n_contact_edges_detected][1] = y;
      result->n_contact_edges_detected++;
    }
    // Check overlap on N parameter
    for (size_t j = 1; j <= piece->N; j++) {
      if ((x - j) == player->playbook->data[i][0] &&
          y == player->playbook->data[i][1]) {
        result->edge_contact_coord[result->n_contact_edges_detected][0] =
            (uint_8)(x - j);
        result->edge_contact_coord[result->n_contact_edges_detected][1] = y;
        result->n_contact_edges_detected++;
      }
    }
    // Check overlap on S parameter
    for (size_t j = 1; j <= piece->S; j++) {
      if ((x + j) == player->playbook->data[i][0] &&
          y == player->playbook->data[i][1]) {
        result->edge_contact_coord[result->n_contact_edges_detected][0] =
            (uint_8)(x + j);
        result->edge_contact_coord[result->n_contact_edges_detected][1] = y;
        result->n_contact_edges_detected++;
      }
    }
    // Check overlap on W parameter
    for (size_t j = 1; j <= piece->W; j++) {
      if (x == player->playbook->data[i][0] &&
          (y - j) == player->playbook->data[i][1]) {
        result->edge_contact_coord[result->n_contact_edges_detected][0] = x;
        result->edge_contact_coord[result->n_contact_edges_detected][1] =
            (uint_8)(y - j);
        result->n_contact_edges_detected++;
      }
    }
    // Check overlap on E parameter
    for (size_t j = 1; j <= piece->E; j++) {
      if (x == player->playbook->data[i][0] &&
          (y + j) == player->playbook->data[i][1]) {
        result->edge_contact_coord[result->n_contact_edges_detected][0] = x;
        result->edge_contact_coord[result->n_contact_edges_detected][1] =
            (uint_8)(y + j);
        result->n_contact_edges_detected++;
      }
    }
    // Check overlap on SW parameter
    if (piece->SW != 0 && (x + piece->SW) == player->playbook->data[i][0] &&
        (y - piece->SW) == player->playbook->data[i][1]) {
      result->edge_contact_coord[result->n_contact_edges_detected][0] =
          x + piece->SW;
      result->edge_contact_coord[result->n_contact_edges_detected][1] =
          y - piece->SW;
      result->n_contact_edges_detected++;
    }
    // Check overlap on SE parameter
    if (piece->SE != 0 && (x + piece->SE) == player->playbook->data[i][0] &&
        (y + piece->SE) == player->playbook->data[i][1]) {
      result->edge_contact_coord[result->n_contact_edges_detected][0] =
          x + piece->SE;
      result->edge_contact_coord[result->n_contact_edges_detected][1] =
          y + piece->SE;
      result->n_contact_edges_detected++;
    }
    // Check overlap on NW parameter
    if (piece->NW != 0 && (x - piece->NW) == player->playbook->data[i][0] &&
        (y - piece->NW) == player->playbook->data[i][1]) {
      result->edge_contact_coord[result->n_contact_edges_detected][0] =
          x - piece->NW;
      result->edge_contact_coord[result->n_contact_edges_detected][1] =
          y - piece->NW;
      result->n_contact_edges_detected++;
    }
    // Check overlap on NE parameter
    if (piece->NE != 0 && (x - piece->NE) == player->playbook->data[i][0] &&
        (y + piece->NE) == player->playbook->data[i][1]) {
      result->edge_contact_coord[result->n_contact_edges_detected][0] =
          x - piece->NE;
      result->edge_contact_coord[result->n_contact_edges_detected][1] =
          y + piece->NW;
      result->n_contact_edges_detected++;
    }
  }

  if (result->n_contact_edges_detected == 0) {
    return FALSE;
  } else {
    return TRUE;
  }
}

// Check if the played piece has a side to side contact with a piece of the
// same
// player.
//
// # Arguments
// * self -> the board to check the play on
// * x -> the row coordinate
// * y -> the column coordinate
// * piece -> the block to set
// * player -> player which is attempting the move
//
// # Return Value
// True if the piece contains side by side contact, false otherwise
static Bool check_side_contact(Board *self, uint_8 x, uint_8 y, Piece *piece,
                               Players *player) {
  // Check contact made by center piece
  if (has_piece(&self->cells[x - 1][y]) == TRUE &&
      get_player_piece(&self->cells[x - 1][y]) == player->player) {
    return TRUE;
  }
  if (has_piece(&self->cells[x + 1][y]) == TRUE &&
      get_player_piece(&self->cells[x + 1][y]) == player->player) {
    return TRUE;
  }
  if (has_piece(&self->cells[x][y - 1]) == TRUE &&
      get_player_piece(&self->cells[x][y - 1]) == player->player) {
    return TRUE;
  }
  if (has_piece(&self->cells[x][y + 1]) == TRUE &&
      get_player_piece(&self->cells[x][y + 1]) == player->player) {
    return TRUE;
  }

  // Check contact made by an N parameter piece
  for (size_t i = 1; i <= piece->N; i++) {
    if (has_piece(&self->cells[x - i - 1][y]) == TRUE &&
        get_player_piece(&self->cells[x - i - 1][y]) == player->player) {
      return TRUE;
    }
    if (has_piece(&self->cells[x - i + 1][y]) == TRUE &&
        get_player_piece(&self->cells[x - i + 1][y]) == player->player) {
      return TRUE;
    }
    if (has_piece(&self->cells[x - i][y - 1]) == TRUE &&
        get_player_piece(&self->cells[x - i][y - 1]) == player->player) {
      return TRUE;
    }
    if (has_piece(&self->cells[x - i][y + 1]) == TRUE &&
        get_player_piece(&self->cells[x - i][y + 1]) == player->player) {
      return TRUE;
    }
  }
  // Check contact made by an S parameter piece
  for (size_t i = 1; i <= piece->S; i++) {
    if (has_piece(&self->cells[x + i - 1][y]) == TRUE &&
        get_player_piece(&self->cells[x + i - 1][y]) == player->player) {
      return TRUE;
    }
    if (has_piece(&self->cells[x + i + 1][y]) == TRUE &&
        get_player_piece(&self->cells[x + i + 1][y]) == player->player) {
      return TRUE;
    }
    if (has_piece(&self->cells[x + i][y - 1]) == TRUE &&
        get_player_piece(&self->cells[x + i][y - 1]) == player->player) {
      return TRUE;
    }
    if (has_piece(&self->cells[x + i][y + 1]) == TRUE &&
        get_player_piece(&self->cells[x + i][y + 1]) == player->player) {
      return TRUE;
    }
  }
  // Check overlap on W parameter
  for (size_t i = 1; i <= piece->W; i++) {
    if (has_piece(&self->cells[x - 1][y - i]) == TRUE &&
        get_player_piece(&self->cells[x - 1][y - i]) == player->player) {
      return TRUE;
    }
    if (has_piece(&self->cells[x + 1][y - i]) == TRUE &&
        get_player_piece(&self->cells[x + 1][y - i]) == player->player) {
      return TRUE;
    }
    if (has_piece(&self->cells[x][y - i - 1]) == TRUE &&
        get_player_piece(&self->cells[x][y - i - 1]) == player->player) {
      return TRUE;
    }
    if (has_piece(&self->cells[x][y - i + 1]) == TRUE &&
        get_player_piece(&self->cells[x][y - i + 1]) == player->player) {
      return TRUE;
    }
  }
  // Check overlap on E parameter
  for (size_t i = 1; i <= piece->E; i++) {
    if (has_piece(&self->cells[x - 1][y + i]) == TRUE &&
        get_player_piece(&self->cells[x - 1][y + i]) == player->player) {
      return TRUE;
    }
    if (has_piece(&self->cells[x + 1][y + i]) == TRUE &&
        get_player_piece(&self->cells[x + 1][y + i]) == player->player) {
      return TRUE;
    }
    if (has_piece(&self->cells[x][y + i - 1]) == TRUE &&
        get_player_piece(&self->cells[x][y + i - 1]) == player->player) {
      return TRUE;
    }
    if (has_piece(&self->cells[x][y + i + 1]) == TRUE &&
        get_player_piece(&self->cells[x][y + i + 1]) == player->player) {
      return TRUE;
    }
  }
  // Check overlap on SW parameter
  if (has_piece(&self->cells[x + piece->SW + 1][y - piece->SW]) == TRUE &&
      get_player_piece(&self->cells[x + piece->SW + 1][y - piece->SW]) ==
          player->player) {
    return TRUE;
  }
  if (has_piece(&self->cells[x + piece->SW - 1][y - piece->SW]) == TRUE &&
      get_player_piece(&self->cells[x + piece->SW - 1][y - piece->SW]) ==
          player->player) {
    return TRUE;
  }
  if (has_piece(&self->cells[x + piece->SW][y - piece->SW - 1]) == TRUE &&
      get_player_piece(&self->cells[x + piece->SW][y - piece->SW - 1]) ==
          player->player) {
    return TRUE;
  }
  if (has_piece(&self->cells[x + piece->SW][y - piece->SW + 1]) == TRUE &&
      get_player_piece(&self->cells[x + piece->SW][y - piece->SW + 1]) ==
          player->player) {
    return TRUE;
  }
  // Check overlap on SE parameter
  if (has_piece(&self->cells[x + piece->SE - 1][y + piece->SE]) == TRUE &&
      get_player_piece(&self->cells[x + piece->SE - 1][y + piece->SE]) ==
          player->player) {
    return TRUE;
  }
  if (has_piece(&self->cells[x + piece->SE + 1][y + piece->SE]) == TRUE &&
      get_player_piece(&self->cells[x + piece->SE + 1][y + piece->SE]) ==
          player->player) {
    return TRUE;
  }
  if (has_piece(&self->cells[x + piece->SE][y + piece->SE - 1]) == TRUE &&
      get_player_piece(&self->cells[x + piece->SE][y + piece->SE - 1]) ==
          player->player) {
    return TRUE;
  }
  if (has_piece(&self->cells[x + piece->SE][y + piece->SE + 1]) == TRUE &&
      get_player_piece(&self->cells[x + piece->SE][y + piece->SE + 1]) ==
          player->player) {
    return TRUE;
  }
  // Check overlap on NW parameter
  if (has_piece(&self->cells[x - piece->NW - 1][y - piece->NW]) == TRUE &&
      get_player_piece(&self->cells[x - piece->NW - 1][y - piece->NW]) ==
          player->player) {
    return TRUE;
  }
  if (has_piece(&self->cells[x - piece->NW + 1][y - piece->NW]) == TRUE &&
      get_player_piece(&self->cells[x - piece->NW + 1][y - piece->NW]) ==
          player->player) {
    return TRUE;
  }
  if (has_piece(&self->cells[x - piece->NW][y - piece->NW - 1]) == TRUE &&
      get_player_piece(&self->cells[x - piece->NW][y - piece->NW - 1]) ==
          player->player) {
    return TRUE;
  }
  if (has_piece(&self->cells[x - piece->NW][y - piece->NW + 1]) == TRUE &&
      get_player_piece(&self->cells[x - piece->NW][y - piece->NW + 1]) ==
          player->player) {
    return TRUE;
  }
  // Check overlap on NE parameter
  if (has_piece(&self->cells[x - piece->NE - 1][y + piece->NE]) == TRUE &&
      get_player_piece(&self->cells[x - piece->NE - 1][y + piece->NE]) ==
          player->player) {
    return TRUE;
  }
  if (has_piece(&self->cells[x - piece->NE + 1][y + piece->NE]) == TRUE &&
      get_player_piece(&self->cells[x - piece->NE + 1][y + piece->NE]) ==
          player->player) {
    return TRUE;
  }
  if (has_piece(&self->cells[x - piece->NE][y + piece->NE - 1]) == TRUE &&
      get_player_piece(&self->cells[x - piece->NE][y + piece->NE - 1]) ==
          player->player) {
    return TRUE;
  }
  if (has_piece(&self->cells[x - piece->NE][y + piece->NE + 1]) == TRUE &&
      get_player_piece(&self->cells[x - piece->NE][y + piece->NE + 1]) ==
          player->player) {
    return TRUE;
  }

  return FALSE;
}

// Check overlap between pieces before performing a move
//
// # Arguments
// * self -> board to place the piece on
// * x -> x coordinate (rows)
// * y -> y coordinate (columns)
// * piece -> piece which is going to be played
//
// # Return Value
// True if there is an overlap between pieces, false otherwise.
static Bool check_overlap(Board *self, uint_8 x, uint_8 y, Piece piece) {
  // Check if piece is already set in x and y coordinates. The for's are
  // required because six pieces will occupy more than one square. In the
  // SW/NW/SE/NE case only one square is used, thus no for's are needed.

  // Check overlap on N parameter
  for (size_t i = 1; i <= piece.N; i++) {
    if (has_piece(&self->cells[x - i][y]) == TRUE) {
      return TRUE;
    }
  }
  // Check overlap on S parameter
  for (size_t i = 1; i <= piece.S; i++) {
    if (has_piece(&self->cells[x + i][y]) == TRUE) {
      return TRUE;
    }
  }
  // Check overlap on W parameter
  for (size_t i = 1; i <= piece.W; i++) {
    if (has_piece(&self->cells[x][y - i]) == TRUE) {
      return TRUE;
    }
  }
  // Check overlap on E parameter
  for (size_t i = 1; i <= piece.E; i++) {
    if (has_piece(&self->cells[x][y + i]) == TRUE) {
      return TRUE;
    }
  }
  // Check overlap on SW parameter
  if (has_piece(&self->cells[x + piece.SW][y - piece.SW]) == TRUE) {
    return TRUE;
  }
  // Check overlap on SE parameter
  if (has_piece(&self->cells[x + piece.SE][y + piece.SE]) == TRUE) {
    return TRUE;
  }
  // Check overlap on NW parameter
  if (has_piece(&self->cells[x - piece.NW][y - piece.NW]) == TRUE) {
    return TRUE;
  }
  // Check overlap on NE parameter
  if (has_piece(&self->cells[x - piece.NE][y + piece.NE]) == TRUE) {
    return TRUE;
  }

  return FALSE;
}

// Check if the first move is correct. The starting move must occupy board
// positions [4,4] or [9,9].
//
// # Arguments
// * x -> x coordinate (rows)
// * y -> y coordinate (columns)
// * piece -> piece selected to be set on the board
//
// # Return Values
// True if the first move is legal, false otherwise
static Bool is_first_move_correct(uint_8 x, uint_8 y, Piece piece) {
  if ((x == 4 && y == 4) || (x == 9 && y == 9)) {
    return TRUE;
  } else if (piece.N < 2 &&
             ((x - piece.N == 4 && y == 4) || (x - piece.N == 9 && y == 9))) {
    return TRUE;
  } else if (piece.S < 2 &&
             ((x + piece.S == 4 && y == 4) || (x + piece.S == 9 && y == 9))) {
    return TRUE;
  } else if (piece.W < 2 &&
             ((x == 4 && y - piece.W == 4) || (x == 9 && y - piece.W == 9))) {
    return TRUE;
  } else if (piece.E < 2 &&
             ((x == 4 && y + piece.E == 4) || (x == 9 && y + piece.E == 9))) {
    return TRUE;
  } else if (piece.N == 2 &&
             ((x - piece.N == 4 && y == 4) || (x - piece.N == 9 && y == 9) ||
              (x - piece.N + 1 == 4 && y == 4) ||
              (x - piece.N + 1 == 9 && y == 9))) {
    return TRUE;
  } else if (piece.S == 2 &&
             ((x + piece.S == 4 && y == 4) || (x + piece.S == 9 && y == 9) ||
              (x + piece.S - 1 == 4 && y == 4) ||
              (x + piece.S - 1 == 9 && y == 9))) {
    return TRUE;
  } else if (piece.W == 2 &&
             ((x == 4 && y - piece.W == 4) || (x == 9 && y - piece.W == 9) ||
              (x == 4 && y - piece.W + 1 == 4) ||
              (x == 9 && y - piece.W + 1 == 9))) {
    return TRUE;
  } else if (piece.E == 2 &&
             ((x == 4 && y + piece.E == 4) || (x == 9 && y + piece.E == 9) ||
              (x == 4 && y + piece.E - 1 == 4) ||
              (x == 9 && y + piece.E - 1 == 9))) {
    return TRUE;
  } else if ((x + piece.SW == 4 && y - piece.SW == 4) ||
             (x + piece.SW == 9 && y - piece.SW == 9)) {
    return TRUE;
  } else if ((x + piece.SE == 4 && y + piece.SE == 4) ||
             (x + piece.SE == 9 && y + piece.SE == 9)) {
    return TRUE;
  } else if ((x - piece.NW == 4 && y - piece.NW == 4) ||
             (x - piece.NW == 9 && y - piece.NW == 9)) {
    return TRUE;
  } else if ((x - piece.NE == 4 && y + piece.NE == 4) ||
             (x - piece.NE == 9 && y + piece.NE == 9)) {
    return TRUE;
  }

  return FALSE;
}

// Check board boundaries before performing a move
//
// # Arguments
// * x -> x coordinate (rows)
// * y -> y coordinate (columns)
// * piece -> piece which is going to be played
//
// # Return Value
// True if the piece is out of bounds, false otherwise.
static Bool check_boundaries(uint_8 x, uint_8 y, Piece piece) {
  uint_8 xlim, ylim;
  uint_8 temp;

  // compute lowest x coordinate
  xlim = x - piece.W;
  temp = xlim;
  if (piece.W == 0) {
    xlim -= piece.NW;
    temp -= piece.SW;
  }
  if (temp > xlim) {
    xlim = temp;
  }

  // compute lowest y coordinate
  ylim = y - piece.N;
  temp = ylim;
  if (piece.N == 0) {
    ylim -= piece.NW;
    temp -= piece.NE;
  }
  if (temp > ylim) {
    ylim = temp;
  }

  // check lowest coordinates
  if (xlim > 13 || ylim > 13) {
    return TRUE;
  }

  // compute highest x coordinate
  xlim = x + piece.E;
  temp = xlim;
  if (piece.E == 0) {
    xlim += piece.NE;
    temp += piece.SE;
  }
  if (temp > xlim) {
    xlim = temp;
  }

  // compute highest y coordinate
  ylim = y + piece.S;
  temp = ylim;
  if (piece.S == 0) {
    ylim += piece.SW;
    temp += piece.SE;
  }
  if (temp > ylim) {
    ylim = temp;
  }

  // check highest coordinates
  if (xlim > 13 || ylim > 13) {
    return TRUE;
  }

  return FALSE;
}

// Print board to stdout
void print_board(Board *self) {
  for (size_t i = 0; i < 15; i++) {
    for (size_t j = 0; j < 15; j++) {
      if (i == 0 && j == 0) {
        printf("    ");
      }

      if (j == 0 && i != 0) {
        if (i < 10) {
          printf(" ");
        }
        printf(" %lu ", i);
      }

      if (i == 0 && j < 14) {
        if (j >= 9) {
          printf(" %lu  ", j + 1);
        } else {
          printf("  %lu  ", j + 1);
        }
      } else if (j != 0 && i != 0) {
        print_cell(&self->cells[i - 1][j - 1]);
      }

      if (j != 14 && i != 0 && j >= 1) {
        printf(":");
      } else if ((i != 0 || j == 14) && j >= 1) {
        printf("\n");
      }
    }
  }

  return;
}
