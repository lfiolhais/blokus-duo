#include <stdio.h>

#include "Pieces.h"

// Monomino
static const Piece A = {.N = 0,
                        .S = 0,
                        .W = 0,
                        .E = 0,
                        .SW = 0,
                        .SE = 0,
                        .NW = 0,
                        .NE = 0,
                        .score = 1,
                        .n_possible_edges = 4,
                        .pe = {
                            {.W = 1, .E = 0, .S = 0, .N = 1},
                            {.W = 0, .E = 1, .S = 0, .N = 1},
                            {.W = 0, .E = 1, .S = 1, .N = 0},
                            {.W = 1, .E = 0, .S = 1, .N = 0},
                        }};

// Domino
static const Piece B = {.N = 0,
                        .S = 1,
                        .W = 0,
                        .E = 0,
                        .SW = 0,
                        .SE = 0,
                        .NW = 0,
                        .NE = 0,
                        .score = 2,
                        .n_possible_edges = 4,
                        .pe = {
                            {.W = 1, .E = 0, .S = 0, .N = 1},
                            {.W = 0, .E = 1, .S = 0, .N = 1},
                            {.W = 0, .E = 1, .S = 2, .N = 0},
                            {.W = 1, .E = 0, .S = 2, .N = 0},
                        }};

// Trominoes
static const Piece C = {.N = 1,
                        .S = 1,
                        .W = 0,
                        .E = 0,
                        .SW = 0,
                        .SE = 0,
                        .NW = 0,
                        .NE = 0,
                        .score = 3,
                        .n_possible_edges = 4,
                        .pe = {
                            {.W = 0, .E = 1, .S = 0, .N = 2},
                            {.W = 1, .E = 0, .S = 0, .N = 2},
                            {.W = 0, .E = 1, .S = 2, .N = 0},
                            {.W = 1, .E = 0, .S = 2, .N = 0},
                        }};

static const Piece D = {.N = 1,
                        .S = 0,
                        .W = 0,
                        .E = 1,
                        .SW = 0,
                        .SE = 0,
                        .NW = 0,
                        .NE = 0,
                        .score = 3,
                        .n_possible_edges = 5,
                        .pe = {
                            {.W = 1, .E = 0, .S = 0, .N = 2},
                            {.W = 0, .E = 1, .S = 0, .N = 2},
                            {.W = 1, .E = 0, .S = 1, .N = 0},
                            {.W = 0, .E = 2, .S = 0, .N = 1},
                            {.W = 0, .E = 2, .S = 1, .N = 0},
                        }};

// Tetrominoes
static const Piece E = {.N = 1,
                        .S = 2,
                        .W = 0,
                        .E = 0,
                        .SW = 0,
                        .SE = 0,
                        .NW = 0,
                        .NE = 0,
                        .score = 4,
                        .n_possible_edges = 4,
                        .pe = {
                            {.W = 1, .E = 0, .S = 0, .N = 2},
                            {.W = 0, .E = 1, .S = 0, .N = 2},
                            {.W = 0, .E = 1, .S = 3, .N = 0},
                            {.W = 1, .E = 0, .S = 3, .N = 0},
                        }};

static const Piece F = {.N = 1,
                        .S = 1,
                        .W = 0,
                        .E = 0,
                        .SW = 1,
                        .SE = 0,
                        .NW = 0,
                        .NE = 0,
                        .score = 4,
                        .n_possible_edges = 5,
                        .pe = {
                            {.W = 1, .E = 0, .S = 0, .N = 2},
                            {.W = 0, .E = 1, .S = 0, .N = 2},
                            {.W = 0, .E = 1, .S = 2, .N = 0},
                            {.W = 2, .E = 0, .S = 0, .N = 0},
                            {.W = 2, .E = 0, .S = 2, .N = 0},
                        }};

static const Piece G = {.N = 1,
                        .S = 1,
                        .W = 0,
                        .E = 1,
                        .SW = 0,
                        .SE = 0,
                        .NW = 0,
                        .NE = 0,
                        .score = 4,
                        .n_possible_edges = 6,
                        .pe = {
                            {.W = 1, .E = 0, .S = 0, .N = 2},
                            {.W = 0, .E = 1, .S = 0, .N = 2},
                            {.W = 0, .E = 2, .S = 0, .N = 1},
                            {.W = 0, .E = 2, .S = 1, .N = 0},
                            {.W = 0, .E = 1, .S = 2, .N = 0},
                            {.W = 1, .E = 0, .S = 2, .N = 0},
                        }};

static const Piece H = {.N = 0,
                        .S = 1,
                        .W = 0,
                        .E = 1,
                        .SW = 0,
                        .SE = 1,
                        .NW = 0,
                        .NE = 0,
                        .score = 4,
                        .n_possible_edges = 4,
                        .pe = {
                            {.W = 1, .E = 0, .S = 0, .N = 1},
                            {.W = 0, .E = 2, .S = 0, .N = 1},
                            {.W = 0, .E = 2, .S = 2, .N = 0},
                            {.W = 1, .E = 0, .S = 2, .N = 0},
                        }};

static const Piece I = {.N = 0,
                        .S = 1,
                        .W = 1,
                        .E = 0,
                        .SW = 0,
                        .SE = 1,
                        .NW = 0,
                        .NE = 0,
                        .score = 4,
                        .n_possible_edges = 6,
                        .pe = {
                            {.W = 2, .E = 0, .S = 0, .N = 1},
                            {.W = 2, .E = 0, .S = 1, .N = 0},
                            {.W = 0, .E = 1, .S = 0, .N = 1},
                            {.W = 1, .E = 0, .S = 2, .N = 0},
                            {.W = 0, .E = 2, .S = 0, .N = 0},
                            {.W = 0, .E = 2, .S = 2, .N = 0},
                        }};

// Pentominoes
static const Piece J = {.N = 2,
                        .S = 2,
                        .W = 0,
                        .E = 0,
                        .SW = 0,
                        .SE = 0,
                        .NW = 0,
                        .NE = 0,
                        .score = 5,
                        .n_possible_edges = 4,
                        .pe = {
                            {.W = 1, .E = 0, .S = 0, .N = 3},
                            {.W = 0, .E = 1, .S = 0, .N = 3},
                            {.W = 1, .E = 0, .S = 3, .N = 0},
                            {.W = 0, .E = 1, .S = 3, .N = 0},
                        }};

static const Piece K = {.N = 2,
                        .S = 1,
                        .W = 0,
                        .E = 0,
                        .SW = 1,
                        .SE = 0,
                        .NW = 0,
                        .NE = 0,
                        .score = 5,
                        .n_possible_edges = 5,
                        .pe = {
                            {.W = 1, .E = 0, .S = 0, .N = 3},
                            {.W = 0, .E = 1, .S = 0, .N = 3},
                            {.W = 0, .E = 1, .S = 2, .N = 0},
                            {.W = 2, .E = 0, .S = 2, .N = 0},
                            {.W = 2, .E = 0, .S = 0, .N = 0},
                        }};

static const Piece L = {.N = 2,
                        .S = 0,
                        .W = 1,
                        .E = 0,
                        .SW = 1,
                        .SE = 0,
                        .NW = 0,
                        .NE = 0,
                        .score = 5,
                        .n_possible_edges = 6,
                        .pe = {
                            {.W = 1, .E = 0, .S = 0, .N = 3},
                            {.W = 0, .E = 1, .S = 0, .N = 3},
                            {.W = 2, .E = 0, .S = 0, .N = 1},
                            {.W = 2, .E = 0, .S = 2, .N = 0},
                            {.W = 0, .E = 0, .S = 2, .N = 0},
                            {.W = 0, .E = 1, .S = 1, .N = 0},
                        }};

static const Piece M = {.N = 1,
                        .S = 1,
                        .W = 1,
                        .E = 0,
                        .SW = 1,
                        .SE = 0,
                        .NW = 0,
                        .NE = 0,
                        .score = 5,
                        .n_possible_edges = 5,
                        .pe = {
                            {.W = 2, .E = 0, .S = 0, .N = 1},
                            {.W = 2, .E = 0, .S = 2, .N = 0},
                            {.W = 0, .E = 1, .S = 2, .N = 0},
                            {.W = 0, .E = 1, .S = 0, .N = 2},
                            {.W = 1, .E = 0, .S = 0, .N = 2},
                        }};

static const Piece N = {.N = 1,
                        .S = 1,
                        .W = 0,
                        .E = 0,
                        .SW = 1,
                        .SE = 0,
                        .NW = 1,
                        .NE = 0,
                        .score = 5,
                        .n_possible_edges = 5,
                        .pe = {
                            {.W = 2, .E = 0, .S = 0, .N = 2},
                            {.W = 0, .E = 1, .S = 0, .N = 2},
                            {.W = 0, .E = 1, .S = 2, .N = 0},
                            {.W = 2, .E = 0, .S = 2, .N = 0},
                            {.W = 2, .E = 0, .S = 0, .N = 0},
                        }};

static const Piece O = {.N = 1,
                        .S = 2,
                        .W = 0,
                        .E = 1,
                        .SW = 0,
                        .SE = 0,
                        .NW = 0,
                        .NE = 0,
                        .score = 5,
                        .n_possible_edges = 6,
                        .pe = {
                            {.W = 1, .E = 0, .S = 0, .N = 2},
                            {.W = 0, .E = 1, .S = 0, .N = 2},
                            {.W = 0, .E = 2, .S = 0, .N = 1},
                            {.W = 0, .E = 2, .S = 1, .N = 0},
                            {.W = 1, .E = 0, .S = 3, .N = 0},
                            {.W = 0, .E = 1, .S = 3, .N = 0},
                        }};

static const Piece P = {.N = 1,
                        .S = 1,
                        .W = 0,
                        .E = 0,
                        .SW = 1,
                        .SE = 1,
                        .NW = 0,
                        .NE = 0,
                        .score = 5,
                        .n_possible_edges = 6,
                        .pe = {
                            {.W = 1, .E = 0, .S = 0, .N = 2},
                            {.W = 0, .E = 1, .S = 0, .N = 2},
                            {.W = 2, .E = 0, .S = 0, .N = 0},
                            {.W = 2, .E = 0, .S = 2, .N = 0},
                            {.W = 0, .E = 2, .S = 0, .N = 0},
                            {.W = 0, .E = 2, .S = 2, .N = 0},
                        }};

static const Piece Q = {.N = 2,
                        .S = 0,
                        .W = 0,
                        .E = 2,
                        .SW = 0,
                        .SE = 0,
                        .NW = 0,
                        .NE = 0,
                        .score = 5,
                        .n_possible_edges = 5,
                        .pe = {
                            {.W = 1, .E = 0, .S = 0, .N = 3},
                            {.W = 0, .E = 1, .S = 0, .N = 3},
                            {.W = 1, .E = 0, .S = 1, .N = 0},
                            {.W = 0, .E = 3, .S = 0, .N = 1},
                            {.W = 0, .E = 3, .S = 1, .N = 0},
                        }};

static const Piece R = {.N = 1,
                        .S = 0,
                        .W = 0,
                        .E = 1,
                        .SW = 0,
                        .SE = 1,
                        .NW = 1,
                        .NE = 0,
                        .score = 5,
                        .n_possible_edges = 7,
                        .pe = {
                            {.W = 2, .E = 0, .S = 0, .N = 2},
                            {.W = 2, .E = 0, .S = 0, .N = 0},
                            {.W = 0, .E = 1, .S = 0, .N = 2},
                            {.W = 0, .E = 2, .S = 0, .N = 1},
                            {.W = 0, .E = 2, .S = 2, .N = 0},
                            {.W = 0, .E = 0, .S = 2, .N = 0},
                            {.W = 1, .E = 0, .S = 1, .N = 0},
                        }};

static const Piece S = {.N = 0,
                        .S = 0,
                        .W = 1,
                        .E = 1,
                        .SW = 0,
                        .SE = 1,
                        .NW = 1,
                        .NE = 0,
                        .score = 5,
                        .n_possible_edges = 6,
                        .pe = {
                            {.W = 2, .E = 0, .S = 0, .N = 2},
                            {.W = 0, .E = 0, .S = 0, .N = 2},
                            {.W = 2, .E = 0, .S = 1, .N = 0},
                            {.W = 0, .E = 2, .S = 0, .N = 1},
                            {.W = 0, .E = 0, .S = 2, .N = 0},
                            {.W = 0, .E = 2, .S = 2, .N = 0},
                        }};

static const Piece T = {.N = 0,
                        .S = 1,
                        .W = 1,
                        .E = 1,
                        .SW = 0,
                        .SE = 0,
                        .NW = 1,
                        .NE = 0,
                        .score = 5,
                        .n_possible_edges = 7,
                        .pe = {
                            {.W = 2, .E = 0, .S = 0, .N = 2},
                            {.W = 0, .E = 0, .S = 0, .N = 2},
                            {.W = 2, .E = 0, .S = 1, .N = 0},
                            {.W = 1, .E = 0, .S = 2, .N = 0},
                            {.W = 0, .E = 1, .S = 2, .N = 0},
                            {.W = 0, .E = 2, .S = 1, .N = 0},
                            {.W = 0, .E = 2, .S = 0, .N = 1},
                        }};

static const Piece U = {.N = 1,
                        .S = 1,
                        .W = 1,
                        .E = 1,
                        .SW = 0,
                        .SE = 0,
                        .NW = 0,
                        .NE = 0,
                        .score = 5,
                        .n_possible_edges = 8,
                        .pe = {
                            {.W = 1, .E = 0, .S = 0, .N = 2},
                            {.W = 0, .E = 1, .S = 0, .N = 2},
                            {.W = 0, .E = 2, .S = 0, .N = 1},
                            {.W = 0, .E = 2, .S = 1, .N = 0},
                            {.W = 0, .E = 1, .S = 2, .N = 0},
                            {.W = 1, .E = 0, .S = 2, .N = 0},
                            {.W = 2, .E = 0, .S = 0, .N = 1},
                            {.W = 2, .E = 0, .S = 1, .N = 0},
                        }};

void print_piece(Piece self) {
  printf("Piece\n");
  printf("\tN: %u\n", self.N);
  printf("\tS: %u\n", self.S);
  printf("\tW: %u\n", self.W);
  printf("\tE: %u\n", self.E);
  printf("\tSW: %u\n", self.SW);
  printf("\tSE: %u\n", self.SE);
  printf("\tNW: %u\n", self.NW);
  printf("\tNE: %u\n", self.NE);
  printf("\tscore: %u\n", self.score);
}

// Perform a rotation on a given piece. TODO: This can be optimized by copy and
// pasting a bunch of code and removing the recursive calls.
//
// # Arguments
// * self -> piece to rotate
// * rotate -> rotation code
//
// # Return Values
// Rotated Piece
static Piece rotate_piece(Piece self, char rotate) {
  // Copy piece into the stack
  Piece piece_2_rotate = self;

  switch (rotate) {
  case '0':
    return piece_2_rotate;
  case '1': {
    // Vertical symmetry
    // Invert east and west
    uint_8 old_E = piece_2_rotate.E;
    piece_2_rotate.E = piece_2_rotate.W;
    piece_2_rotate.W = old_E;
    // Invert north east and north west
    uint_8 old_NE = piece_2_rotate.NE;
    piece_2_rotate.NE = piece_2_rotate.NW;
    piece_2_rotate.NW = old_NE;
    // Invert south east and south west
    uint_8 old_SE = piece_2_rotate.SE;
    piece_2_rotate.SE = piece_2_rotate.SW;
    piece_2_rotate.SW = old_SE;

    return piece_2_rotate;
  }
  case '2': {
    // Rotate -90º
    // east to south, south to west, west to north, north to east
    uint_8 old_E = piece_2_rotate.E;
    uint_8 old_S = piece_2_rotate.S;
    uint_8 old_W = piece_2_rotate.W;
    uint_8 old_N = piece_2_rotate.N;
    piece_2_rotate.S = old_E;
    piece_2_rotate.W = old_S;
    piece_2_rotate.N = old_W;
    piece_2_rotate.E = old_N;

    // northwest to northeast, northeast to southeast, southeast to southwest,
    // southwest to northwest
    uint_8 old_NW = piece_2_rotate.NW;
    uint_8 old_NE = piece_2_rotate.NE;
    uint_8 old_SE = piece_2_rotate.SE;
    uint_8 old_SW = piece_2_rotate.SW;
    piece_2_rotate.NE = old_NW;
    piece_2_rotate.SE = old_NE;
    piece_2_rotate.SW = old_SE;
    piece_2_rotate.NW = old_SW;

    return piece_2_rotate;
  }
  case '3': {
    // Vertical symmetry of -90º rotation
    Piece ninety_rotated_piece = rotate_piece(piece_2_rotate, '2');
    Piece final_rotated_piece = rotate_piece(ninety_rotated_piece, '1');

    return final_rotated_piece;
  }
  case '4': {
    // Rotate 180º
    Piece ninety_rotated_piece = rotate_piece(piece_2_rotate, '2');
    Piece one_eighty_rotated_piece = rotate_piece(ninety_rotated_piece, '2');

    return one_eighty_rotated_piece;
  }
  case '5': {
    // Vertical symmetry of 180º rotation
    Piece one_eighty_rotated_piece = rotate_piece(piece_2_rotate, '4');
    Piece final_rotated_piece = rotate_piece(one_eighty_rotated_piece, '1');

    return final_rotated_piece;
  }
  case '6': {
    // Rotate 90º
    Piece one_eighty_rotated_piece = rotate_piece(piece_2_rotate, '4');
    Piece final_rotated_piece = rotate_piece(one_eighty_rotated_piece, '2');

    return final_rotated_piece;
  }
  case '7': {
    // Vertical symmetry of 90º rotation
    Piece ninety_rotated_piece = rotate_piece(piece_2_rotate, '6');
    Piece final_rotated_piece = rotate_piece(ninety_rotated_piece, '1');

    return final_rotated_piece;
  }
  default:
    return piece_2_rotate;
  }
}

// Get piece from library and perform the correct rotation
//
// # Arguments
// * piece -> The selected piece to play
// * rotation -> The rotation of the selected piece
//
// # Return Value
// The selected piece with the specified rotation
Piece get_piece(char piece, char rotation) {
  switch (piece) {
  case 'a':
    return rotate_piece(A, rotation);
  case 'b':
    return rotate_piece(B, rotation);
  case 'c':
    return rotate_piece(C, rotation);
  case 'd':
    return rotate_piece(D, rotation);
  case 'e':
    return rotate_piece(E, rotation);
  case 'f':
    return rotate_piece(F, rotation);
  case 'g':
    return rotate_piece(G, rotation);
  case 'h':
    return rotate_piece(H, rotation);
  case 'i':
    return rotate_piece(I, rotation);
  case 'j':
    return rotate_piece(J, rotation);
  case 'k':
    return rotate_piece(K, rotation);
  case 'l':
    return rotate_piece(L, rotation);
  case 'm':
    return rotate_piece(M, rotation);
  case 'n':
    return rotate_piece(N, rotation);
  case 'o':
    return rotate_piece(O, rotation);
  case 'p':
    return rotate_piece(P, rotation);
  case 'q':
    return rotate_piece(Q, rotation);
  case 'r':
    return rotate_piece(R, rotation);
  case 's':
    return rotate_piece(S, rotation);
  case 't':
    return rotate_piece(T, rotation);
  case 'u':
    return rotate_piece(U, rotation);
  default:
    return A;
  }
}

// Get piece score from library
//
// # Arguments
// * piece -> The selected piece to play
//
// # Return Value
// The selected piece score
uint_8 get_piece_score(char piece) {
  switch (piece) {
  case 'a':
    return A.score;
  case 'b':
    return B.score;
  case 'c':
    return C.score;
  case 'd':
    return D.score;
  case 'e':
    return E.score;
  case 'f':
    return F.score;
  case 'g':
    return G.score;
  case 'h':
    return H.score;
  case 'i':
    return I.score;
  case 'j':
    return J.score;
  case 'k':
    return K.score;
  case 'l':
    return L.score;
  case 'm':
    return M.score;
  case 'n':
    return N.score;
  case 'o':
    return O.score;
  case 'p':
    return P.score;
  case 'q':
    return Q.score;
  case 'r':
    return R.score;
  case 's':
    return S.score;
  case 't':
    return T.score;
  case 'u':
    return U.score;
  default:
    return A.score;
  }
}

// Get number of possible edges from a piece
//
// # Arguments
// * piece -> The selected piece to play
//
// # Return Value
// The number of possible edges
uint_8 get_piece_n_edges(char piece) {
  switch (piece) {
  case 'a':
    return A.n_possible_edges;
  case 'b':
    return B.n_possible_edges;
  case 'c':
    return C.n_possible_edges;
  case 'd':
    return D.n_possible_edges;
  case 'e':
    return E.n_possible_edges;
  case 'f':
    return F.n_possible_edges;
  case 'g':
    return G.n_possible_edges;
  case 'h':
    return H.n_possible_edges;
  case 'i':
    return I.n_possible_edges;
  case 'j':
    return J.n_possible_edges;
  case 'k':
    return K.n_possible_edges;
  case 'l':
    return L.n_possible_edges;
  case 'm':
    return M.n_possible_edges;
  case 'n':
    return N.n_possible_edges;
  case 'o':
    return O.n_possible_edges;
  case 'p':
    return P.n_possible_edges;
  case 'q':
    return Q.n_possible_edges;
  case 'r':
    return R.n_possible_edges;
  case 's':
    return S.n_possible_edges;
  case 't':
    return T.n_possible_edges;
  case 'u':
    return U.n_possible_edges;
  default:
    return A.n_possible_edges;
  }
}

// Perform a rotation on given edges. TODO: This can be optimized by copy and
// pasting a bunch of code and removing the recursive calls.
//
// # Arguments
// * self -> edges to rotate
// * rotate -> rotation code
//
// # Return Values
// Rotated Edges
PossibleEdges rotate_piece_edges(PossibleEdges self, char rotation_code) {
  // Copy piece into the stack
  PossibleEdges edges_2_rotate = self;

  switch (rotation_code) {
  case '0':
    return self;
  case '1': {
    // Vertical symmetry
    // Invert east and west
    uint_8 old_E = edges_2_rotate.E;
    edges_2_rotate.E = edges_2_rotate.W;
    edges_2_rotate.W = old_E;

    return edges_2_rotate;
  }
  case '2': {
    // Rotate -90º
    // east to south, south to west, west to north, north to east
    uint_8 old_E = edges_2_rotate.E;
    uint_8 old_S = edges_2_rotate.S;
    uint_8 old_W = edges_2_rotate.W;
    uint_8 old_N = edges_2_rotate.N;
    edges_2_rotate.S = old_E;
    edges_2_rotate.W = old_S;
    edges_2_rotate.N = old_W;
    edges_2_rotate.E = old_N;

    return edges_2_rotate;
  }
  case '3': {
    // Vertical symmetry of -90º rotation
    PossibleEdges ninety_rotated_piece =
        rotate_piece_edges(edges_2_rotate, '2');
    PossibleEdges final_rotated_piece =
        rotate_piece_edges(ninety_rotated_piece, '1');

    return final_rotated_piece;
  }
  case '4': {
    // Rotate -180º
    PossibleEdges ninety_rotated_piece =
        rotate_piece_edges(edges_2_rotate, '2');
    PossibleEdges one_eighty_rotated_piece =
        rotate_piece_edges(ninety_rotated_piece, '2');

    return one_eighty_rotated_piece;
  }
  case '5': {
    // Vertical symmetry of 180º rotation
    PossibleEdges one_eighty_rotated_piece =
        rotate_piece_edges(edges_2_rotate, '4');
    PossibleEdges final_rotated_piece =
        rotate_piece_edges(one_eighty_rotated_piece, '1');

    return final_rotated_piece;
  }
  case '6': {
    // Rotate 90º
    PossibleEdges one_eighty_rotated_piece =
        rotate_piece_edges(edges_2_rotate, '4');
    PossibleEdges final_rotated_piece =
        rotate_piece_edges(one_eighty_rotated_piece, '2');

    return final_rotated_piece;
  }
  case '7': {
    // Vertical symmetry of 90º rotation
    PossibleEdges ninety_rotated_piece =
        rotate_piece_edges(edges_2_rotate, '6');
    PossibleEdges final_rotated_piece =
        rotate_piece_edges(ninety_rotated_piece, '1');

    return final_rotated_piece;
  }
  default:
    return edges_2_rotate;
  }
}

// Get possible edges from a piece. You should get the number of possible edges
// for a piece before getting the pointer to the edge data.
//
// # Arguments
// * piece -> The selected piece to play
//
// # Return Value
// A pointer to an array of possible edges for a piece
const PossibleEdges *get_piece_edges(char piece) {
  switch (piece) {
  case 'a':
    return A.pe;
  case 'b':
    return B.pe;
  case 'c':
    return C.pe;
  case 'd':
    return D.pe;
  case 'e':
    return E.pe;
  case 'f':
    return F.pe;
  case 'g':
    return G.pe;
  case 'h':
    return H.pe;
  case 'i':
    return I.pe;
  case 'j':
    return J.pe;
  case 'k':
    return K.pe;
  case 'l':
    return L.pe;
  case 'm':
    return M.pe;
  case 'n':
    return N.pe;
  case 'o':
    return O.pe;
  case 'p':
    return P.pe;
  case 'q':
    return Q.pe;
  case 'r':
    return R.pe;
  case 's':
    return S.pe;
  case 't':
    return T.pe;
  case 'u':
    return U.pe;
  default:
    return A.pe;
  }
}
