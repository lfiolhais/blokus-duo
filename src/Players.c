#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "Pieces.h"
#include "Players.h"
#include "misc.h"

////////////////////////////////////////////////////////////////////////////////
// Playbook Implementation
////////////////////////////////////////////////////////////////////////////////

// Create a new playbook
//
// Exit:
// Will exit if either of the mallocs fail.
static Playbook *new_playbook(void) {
  Playbook *playbook = (Playbook *)malloc(sizeof(Playbook));

  if (playbook == NULL) {
    printerr("Failed to allocate memory to Playbook. Exiting.\n");
    exit(1);
  }

  // Set parameters
  playbook->size = 32;
  playbook->count = 0;

  playbook->data = (uint_8 **)malloc(sizeof(uint_8 *) * playbook->size);

  if (playbook->data == NULL) {
    printerr("Failed to allocate memory to Playbook data. Exiting.\n");
    exit(1);
  }

  for (size_t i = 0; i < playbook->size; i++) {
    playbook->data[i] = (uint_8 *)malloc(sizeof(uint_8) * 2);

    if (playbook->data[i] == NULL) {
      printerr("Failed to allocate memory to Playbook data. Exiting.\n");
      exit(1);
    }
  }

  return playbook;
}

// Add a set of coordinates x (rows) and y (columns) to the playbook vector.
//
// # Arguments
// * self -> the playbook to store the coordinates on
// * x -> rows coordinates
// * y -> columns coordinates
//
// # Exit
// Will exit when expanding memory fails.
void playbook_add(Playbook *self, uint_8 x, uint_8 y) {
  // If the size of the vector is equal to the current index count, then we need
  // to reallocate memory to make space for more data. Since reallocation is
  // expensive, we will always double the size of the vector.
  if (self->size == self->count) {
    self->size *= 2;
    uint_8 **tmp_data = realloc(self->data, sizeof(uint_8 *) * self->size);

    if (tmp_data == NULL) {
      printerr("Failed to reallocate memory to Playbook data. Exiting.\n");
      exit(1);
    }

    for (size_t i = self->size / 2; i < self->size; i++) {
      tmp_data[i] = (uint_8 *)malloc(sizeof(uint_8) * 2);

      if (tmp_data[i] == NULL) {
        printerr("Failed to allocate memory to Playbook data. Exiting.\n");
        exit(1);
      }
    }

    self->data = tmp_data;
  }

  // Store data
  self->data[self->count][0] = x;
  self->data[self->count][1] = y;

  // Increment count
  self->count++;

  return;
}

// Get the values of playbook at index. If the passed index is invalid a NULL
// pointer is returned.
//
// # Arguments
// * self -> the playbook to get the index from
// * index -> index of the data
uint_8 *playbook_get(Playbook *self, uint_8 index) {
  if (index < self->size) {
    return self->data[index];
  }
  return NULL;
}

// Delete from the playbook the coordinates at index.
//
// # Arguments
// * self -> the playbook to delete the index from
// * index -> index to delete from
void playbook_delete(Playbook *self, uint_16 index) {
  if (index >= self->count) {
    return;
  }

  uint_8 *tmp = self->data[index];
  for (size_t i = index; i < (uint_16)(self->count - 1); i++) {
    self->data[i] = self->data[i + 1];
  }
  self->data[self->count - 1] = tmp;

  self->count--;

  return;
}

// Check if a pair of edge coordinates are in the playbook
//
// # Arguments
// * self -> playbook to search index
// * x -> x coordinate
// * y -> y coordinate
//
// # Return Value
// True if in the playbook, false otherwise
static Bool in_playbook(Playbook *self, uint_8 x, uint_8 y) {
  for (size_t i = 0; i < self->count; i++) {
    if (self->data[i][0] == x && self->data[i][1] == y) {
      return TRUE;
    }
  }
  return FALSE;
}

// Get the current size of the playbook
//
// # Arguments
// * self -> the playbook to check the size of
//
// # Return Value
// The size of the playbook
uint_16 playbook_len(Playbook *self) { return self->count; }

static void print_playbook(Playbook *self) {
  printf("\tdata: [");
  for (size_t i = 0; i < self->count; i++) {
    printf("(%u, %u)", self->data[i][1] + 1, self->data[i][0] + 1);
    if (i != (uint_16)(self->count - 1)) {
      printf(", ");
    }
  }
  printf("]\n");

  return;
}

////////////////////////////////////////////////////////////////////////////////
// Players Implementation
////////////////////////////////////////////////////////////////////////////////

// Update a player score after playing a piece
void player_update_score(Players *self, char piece) {
  self->score += get_piece_score(piece);
  return;
}

// Update possible edges from the move made
//
// # Arguments
// * self -> player to store the edges in
// * piece -> the block played
// * rotate_code -> the rotation mode of the block
// * x_coord -> the center row of the block
// * y_coord -> the center column of the block
//
// # Exit
// Will exit if the player's playbook fails to expand its size.
void player_update_edges(Players *self, Players *opponent, char piece_char,
                         char rotate_code, char x_coord, char y_coord,
                         uint_8 edge_contact_coord[8][2],
                         uint_8 n_contact_edges_detected, Board *board) {
  // Convert from char to integer
  uint_8 x = char_2_int(x_coord);
  uint_8 y = char_2_int(y_coord);

  // Get number of edges for the move
  uint_8 n_edges = get_piece_n_edges(piece_char);
  const PossibleEdges *edges = get_piece_edges(piece_char);

  // Generates future plays
  for (size_t i = 0; i < n_edges; i++) {
    PossibleEdges rotated_edge = rotate_piece_edges(edges[i], rotate_code);
    uint_8 edge_x = x + rotated_edge.S - rotated_edge.N;
    uint_8 edge_y = y + rotated_edge.E - rotated_edge.W;
    // Add edges
    if (has_piece(&board->cells[edge_x][edge_y]) == FALSE &&
        in_playbook(self->playbook, edge_x, edge_y) == FALSE) {
      playbook_add(self->playbook, edge_x, edge_y);
    }
  }

  // Remove edges already occupied by the current block. Search for the edge
  // occupied by the block
  uint_16 delete_list[8];
  uint_8 delete_index = 0;
  for (size_t i = 0; i < n_contact_edges_detected; i++) {
    for (uint_16 j = 0; j < self->playbook->count; j++) {
      uint_8 *edge_coord = self->playbook->data[j];
      if (edge_coord[0] == edge_contact_coord[i][0] &&
          edge_coord[1] == edge_contact_coord[i][1]) {
        delete_list[delete_index] = j;
        delete_index++;
      }
    }
  }

  for (size_t i = delete_index; i > 0; i--) {
    playbook_delete(self->playbook, delete_list[i - 1]);
  }

  // Remove edge coordinates which were occupied from opponent's playbook
  delete_index = 0;
  for (uint_16 j = 0; j < opponent->playbook->count; j++) {
    uint_8 *edge_coord = opponent->playbook->data[j];
    if (has_piece(&board->cells[edge_coord[0]][edge_coord[1]]) == TRUE) {
      delete_list[delete_index] = j;
      delete_index++;
    }
  }

  for (size_t i = delete_index; i > 0; i--) {
    playbook_delete(opponent->playbook, delete_list[i - 1]);
  }

  return;
}

// Update player pieces
//
// # Arguments
// * player => the player to update
// * piece_char => the piece to remove
void player_update_pieces(Players *player, char piece_char) {
  player->pieces = player->pieces ^ (0x00000001 << (piece_char - 'a'));
  return;
}

// Get number of pieces left in the playbook.
//
// # Arguments
// * self => player to inspect
//
// # Return Value
// Number of pieces available
uint_32 player_get_n_pieces(Players *self) {
  uint_32 i = self->pieces;

  i = i - ((i >> 1) & 0x55555555);
  i = (i & 0x33333333) + ((i >> 2) & 0x33333333);
  return (((i + (i >> 4)) & 0x0F0F0F0F) * 0x01010101) >> 24;
}

// Create a new player
//
// # Exit
// Will exit if the player or playbook allocation fails.
Players *new_player(Player player_id) {
  // Create player struct
  Players *player = (Players *)malloc(sizeof(Players));

  if (player == NULL) {
    printerr("Failed to allocate memory to Player. Exiting.\n");
    exit(1);
  }

  // Set parameters
  player->score = 0;
  player->pieces = 0x001fffff;
  player->playbook = new_playbook();
  player->player = player_id;

  return player;
}

void print_player(Players *self) {
  printf("Player %d\n", self->player);
  printf("\tscore: %u\n", self->score);
  print_playbook(self->playbook);
  return;
}
