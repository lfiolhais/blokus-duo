/* #include <math.h> */
#include <stdio.h>
#include <stdlib.h>

#include "Tree.h"

// Prototypes
static Node *generate_children(size_t n_corners, size_t n_available_pieces,
                               Players *self, Bool first_play, Board *board,
                               char init_pos, Players *opponent);
static void get_center_block(Bool first_play, size_t coordinate,
                             Node *local_node, char init_pos, Players *player,
                             size_t edge_index);
static char get_piece_from_offset(size_t current_piece_number, Players *player);
static int_32 mean_dist_to_opp_edges(Node *local_node, Players *opponent,
                                     Bool first_play, char init_pos);
static uint_32 blocking_opp_edge(Node *local_node, Players *opponent,
                                 size_t coordenate);
static int_32 smallest_opponent_score(Node *local_node, Players *opponent);
static int_32 block_largest_open_area(Node *local_node, Players *opponent);

// Get the best move
//
// # Arguments
// * first_play => is this the first play of the game
// * self       => the player which will generate the possible moves
// * new_root   => starting root. If it's the first play create a node,
//                 otherwise use the one provided.
// * board      => game board
//
// # Return Value
// New game node with all possible moves
Node *get_best_move(Bool first_play, Players *player, Board *board,
                    char init_pos, Players *opponent) {
  size_t n_corners;
  size_t n_available_pieces;

  if (first_play == TRUE) {
    // At the beginning there are two possible places to set a piece
    n_corners = 1;
    // Number of available pieces, at the beginning there are 21 pieces.
    n_available_pieces = 21;
  } else {
    n_corners = player->playbook->count;
    n_available_pieces = player_get_n_pieces(player);
#ifdef DEBUG
    printf("There are %zu available pieces\n", n_available_pieces);
#endif
  }

  Node *best_node = generate_children(n_corners, n_available_pieces, player,
                                      first_play, board, init_pos, opponent);

  return best_node;
}

// Generate all possible moves for a given node. This function updates the
// parent node with all possible moves.
//
// # Arguments
// * n_corners          => number of corners the player can play in
// * n_available_pieces => number of available pieces
// * player               => the player which will generate the possible moves
// * first_play         => is this the first play of the game
// * board              => game board
//
// # Return Value
// Number of moves generated
static Node *generate_children(size_t n_corners, size_t n_available_pieces,
                               Players *player, Bool first_play, Board *board,
                               char init_pos, Players *opponent) {
  Node *best_node = NULL;

  // Create local auxiliary
  Node *local_node_aux_1 = (Node *)malloc(sizeof(Node));
  if (local_node_aux_1 == NULL) {
    printerr("Failed to allocate memory for tree node.\n");
    exit(1);
  }

  Node *local_node_aux_2 = (Node *)malloc(sizeof(Node));
  if (local_node_aux_2 == NULL) {
    printerr("Failed to allocate memory for tree node.\n");
    exit(1);
  }

  Node *local_node = local_node_aux_1;

  // Number of available corners or starting places
  for (size_t i = 0; i < n_corners; i++) {
    // Number of available pieces
    for (size_t j = 0; j < n_available_pieces; j++) {
      // 8 rotations per piece (Always)
      for (size_t k = 0; k < 8; k++) {
        // Try each edge of piece, a piece can only have nine edges, center +
        // all edges (N+E+W+S+NW+NE+SW+SW)
        for (size_t l = 0; l < 9; l++) {
          // Get piece and rotation
          local_node->piece = get_piece_from_offset(j, player);
          local_node->rotation = k + '0';

          // Create all center block combinations
          get_center_block(first_play, l, local_node, init_pos, player, i);

          // Score should be piece score + distance to opponent's starting
          // position + distance to available edge (a distance of 0 is max score
          // and it should remove an edge which faces the largest open space) +
          // blocking edge + smallest score the oponent can have
          local_node->score =
              (int_32)get_piece_score(local_node->piece) +
              (int_32)mean_dist_to_opp_edges(local_node, opponent, first_play,
                                             init_pos) +
              (int_32)blocking_opp_edge(local_node, opponent, l) +
              (int_32)smallest_opponent_score(local_node, opponent) +
              (int_32)block_largest_open_area(local_node, opponent);

          // If its a valid move add to the children list, otherwise skip it->
          ValidateMoveResult result =
              validate_move(board, first_play, player, local_node->piece,
                            local_node->y, local_node->x, local_node->rotation);
          if (result.tag == ValidateMoveTag_Ok &&
              (best_node == NULL || best_node->score < local_node->score)) {
            best_node = local_node;
            if (local_node == local_node_aux_1) {
              local_node = local_node_aux_2;
            } else {
              local_node = local_node_aux_1;
            }
          }
        }
      }
    }
  }

  if (best_node == local_node_aux_1) {
    free(local_node_aux_2);
  } else {
    free(local_node_aux_1);
  }

  return best_node;
}

// Get the center block of the piece to set.
//
// # Arguments
// * first_play => is this the first play
// * coordinate => coordinate to try center block
// * local_node => The node that contains the play
// * init_pos   => initial position (only read if first_play is true)
// * player     => the player which will play
// * edge_index => current edge index in the playbook
//
// # Return Value
// Get coordinates of center block
static void get_center_block(Bool first_play, size_t coordinate,
                             Node *local_node, char init_pos, Players *player,
                             size_t edge_index) {
  Piece piece = get_piece(local_node->piece, local_node->rotation);
  uint_8 x, y;
  if (first_play == TRUE) {
    x = char_2_int(init_pos);
    y = char_2_int(init_pos);
  } else {
    x = player->playbook->data[edge_index][1];
    y = player->playbook->data[edge_index][0];
  }

  switch (coordinate) {
  case 0:
    // Center
    local_node->x = int_2_char(x);
    local_node->y = int_2_char(y);
    break;
  case 1:
    // North
    local_node->x = int_2_char(x - piece.N);
    local_node->y = int_2_char(y);
    break;
  case 2:
    // South
    local_node->x = int_2_char(x + piece.S);
    local_node->y = int_2_char(y);
    break;
  case 3:
    // East
    local_node->x = int_2_char(x);
    local_node->y = int_2_char(y + piece.E);
    break;
  case 4:
    // West
    local_node->x = int_2_char(x);
    local_node->y = int_2_char(y - piece.W);
    break;
  case 5:
    // Northwest
    local_node->x = int_2_char(x - piece.NW);
    local_node->y = int_2_char(y - piece.NW);
    break;
  case 6:
    // Northeast
    local_node->x = int_2_char(x - piece.NE);
    local_node->y = int_2_char(y + piece.NE);
    break;
  case 7:
    // Southwest
    local_node->x = int_2_char(x + piece.SW);
    local_node->y = int_2_char(y - piece.SW);
    break;
  case 8:
    // Southeast
    local_node->x = int_2_char(x + piece.SE);
    local_node->y = int_2_char(y + piece.SE);
    break;
  default:
    printf("Ah boy... This shouldn't happen.\n");
  }

  return;
}

// Get piece depending on piece number. TODO: Store this elsewhere so that we
// don't have to process a for every time a piece tries to be played.
//
// # Arguments
// * current_piece_number => ???
// * player               => the player which will play
//
// # Return Value
// Piece character code
static char get_piece_from_offset(size_t current_piece_number,
                                  Players *player) {
  uint_32 offset = 0;
  for (size_t w = current_piece_number; w < 21; w++) {
    if (((player->pieces >> current_piece_number) & 0x00000001) == 1) {
      offset = w;
      break;
    }
  }
  return 'a' + offset;
}

static int_32 block_largest_open_area(Node *local_node, Players *opponent) {
  uint_32 opp_pb_len = playbook_len(opponent->playbook);

  if (opp_pb_len == 0) {
    return 0;
  }

  const PossibleEdges *edges = get_piece_edges(local_node->piece);
  uint_8 n_edges = get_piece_n_edges(local_node->piece);
  uint_8 x = char_2_int(local_node->x);
  uint_8 y = char_2_int(local_node->y);

  int_32 mean_block = 0;
  for (size_t i = 0; i < n_edges; i++) {
    PossibleEdges rotated_edge =
        rotate_piece_edges(edges[i], local_node->rotation);
    uint_8 edge_x = 14 - (x + rotated_edge.S - rotated_edge.N);
    uint_8 edge_y = 14 - (y + rotated_edge.E - rotated_edge.W);

    for (size_t j = 0; j < opp_pb_len; j++) {
      uint_8 opp_edge_x = 14 - opponent->playbook->data[j][1];
      uint_8 opp_edge_y = 14 - opponent->playbook->data[j][0];

      if (opp_edge_x > edge_x && abs(edge_x - opp_edge_x) < 5) {
        mean_block += 100000;
      }

      if (opp_edge_y > edge_y && abs(edge_y - opp_edge_y) < 5) {
        mean_block += 100000;
      }
    }
  }

  return mean_block;
}

// Try to find the move which limits the core of the opponent
static int_32 smallest_opponent_score(Node *local_node, Players *opponent) {
  return (local_node->score + opponent->playbook->count) * 0;
}

// Scoring heuristic to check if one of our pieces blocks one or more of the
// opponent's edges.
static uint_32 blocking_opp_edge(Node *local_node, Players *opponent,
                                 size_t coordinate) {
  uint_32 pb_len = playbook_len(opponent->playbook);

  if (pb_len == 0) {
    return 0;
  }

  Piece piece = get_piece(local_node->piece, local_node->rotation);

  uint_32 x = 0;
  uint_32 y = 0;

  switch (coordinate) {
  case 0:
    // Center
    x = char_2_int(local_node->x);
    y = char_2_int(local_node->y);
    break;
  case 1:
    // North
    x = char_2_int(local_node->x) + piece.N;
    y = char_2_int(local_node->y);
    break;
  case 2:
    // South
    x = char_2_int(local_node->x) - piece.S;
    y = char_2_int(local_node->y);
    break;
  case 3:
    // East
    x = char_2_int(local_node->x);
    y = char_2_int(local_node->y) - piece.E;
    break;
  case 4:
    // West
    x = char_2_int(local_node->x);
    y = char_2_int(local_node->y) + piece.W;
    break;
  case 5:
    // Northwest
    x = char_2_int(local_node->x) + piece.NW;
    y = char_2_int(local_node->y) + piece.NW;
    break;
  case 6:
    // Northeast
    x = char_2_int(local_node->x) + piece.NE;
    y = char_2_int(local_node->y) - piece.NE;
    break;
  case 7:
    // Southwest
    x = char_2_int(local_node->x) - piece.SW;
    y = char_2_int(local_node->y) + piece.SW;
    break;
  case 8:
    // Southeast
    x = char_2_int(local_node->x) - piece.SE;
    y = char_2_int(local_node->y) - piece.SE;
    break;
  default:
    printf("Ah boy... This shouldn't happen.\n");
  }

  uint_32 accumulation = 0;
  for (size_t i = 0; i < pb_len; i++) {
    for (size_t j = 0; j < 9; j++) {
      switch (j) {
      case 0:
        // Center
        x = x;
        y = y;
        break;
      case 1:
        // North
        x = x - piece.N;
        y = y;
        break;
      case 2:
        // South
        x = x + piece.S;
        y = y;
        break;
      case 3:
        // East
        x = x;
        y = y + piece.E;
        break;
      case 4:
        // West
        x = x;
        y = y - piece.W;
        break;
      case 5:
        // Northwest
        x = x - piece.NW;
        y = y - piece.NW;
        break;
      case 6:
        // Northeast
        x = x - piece.NE;
        y = y + piece.NE;
        break;
      case 7:
        // Southwest
        x = x + piece.SW;
        y = y - piece.SW;
        break;
      case 8:
        // Southeast
        x = x + piece.SE;
        y = y + piece.SE;
        break;
      default:
        printf("Ah boy... This shouldn't happen.\n");
      }

      // Center
      if (x == opponent->playbook->data[i][1] &&
          y == opponent->playbook->data[i][0]) {
        accumulation += 1000;
      }
    }
  }

  return accumulation;
}

static uint_32 my_sqrt(uint_32 num) {
  uint_32 x = 1 << 16;
  uint_32 y = 0;
  
  if (num == 0) return 0;
  
  while(1) {
    y = (x + (num/x)) >> 1;
    if (y >= x) {
      break;
    }
    x = y;
  }
  
  return x;
}

// Scoring heuristic: calculate the mean distance to all of opponent's edges.
// The shorter the distance, the higher the score.
static int_32 mean_dist_to_opp_edges(Node *local_node, Players *opponent,
                                     Bool first_play, char init_pos) {
  uint_16 pb_len = playbook_len(opponent->playbook);

  if (first_play == TRUE) {
    uint_8 opp_init_pos = 0;
    if (init_pos == 'a') {
      opp_init_pos = 4;
    } else {
      opp_init_pos = 9;
    }

    int_32 delta_x = char_2_int(local_node->x) - opp_init_pos;
    int_32 delta_y = char_2_int(local_node->y) - opp_init_pos;
    int_32 x = (delta_x * delta_x + delta_y * delta_y);
    /* printf("x is %d\n", (int)sqrt(x)); */
    x = (int_32)(my_sqrt((uint_32)x));
    if (x == 0) {
      return 0x7fffffff;
    } else {
      return 1000000 / x;
    }
  }

  if (pb_len == 0) {
    return 0;
  }

  uint_32 accumulation = 0;
  for (size_t i = 0; i < pb_len; i++) {
    // Absolute distance
    int_32 delta_x = char_2_int(local_node->x) - opponent->playbook->data[i][1];
    int_32 delta_y = char_2_int(local_node->y) - opponent->playbook->data[i][0];

    // Calculate diagonal
    int_32 x = (delta_x * delta_x + delta_y * delta_y);
    accumulation += my_sqrt((uint_32)x);
  }
  
  // Return mean
  int_32 mean = 1000000 * pb_len;
  if (accumulation == 0) {
    return mean;
  }
  mean /= accumulation;
  return mean;
}
