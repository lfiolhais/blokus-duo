\documentclass{IEEEconfA4}

\usepackage[latin1]{inputenc}
\usepackage[dvips]{graphicx}
\usepackage{epsfig}
\usepackage{amsfonts, color}
\usepackage{enumitem}
\usepackage{amsmath}
\usepackage{subcaption}
\usepackage{eurosym}
\usepackage{url}
\usepackage{tabularx}

\captionsetup{compatibility=false}

%------------------------------------------------------------------------------
\begin{document}

\title{A Simple Software Approach to a Blokus Duo Player}

% For authors with different affiliations
\author{Lu\'is Fiolhais, Jo\~{a}o Lopes\\
  \begin{affiliation}
    INESC-ID Lisboa / T\'ecnico Lisboa \\
    \email{luis.azenhas.fiolhais@tecnico.ulisboa.pt}
  \end{affiliation}}

\date{}

\maketitle

%------------------------------------------------------------------------------

\begin{abstract}
In this work we present a simple software only Blokus-Duo player. This
player does not use the traditional implementation of a game tree
proceeded by a miniMax with alpha-beta pruning search. Instead we
opted to study which heuristics limit the opponent's playing
area. Therefore, we propose four heuristics which aim to confine the
opponent to a controllable playing area. The proposed software uses
at most $15kB$ and runs on a FPGA plus CPU SoC.
\end{abstract}

{\bf Keywords:} blokus-duo, embedded algorithms, heuristic strategy.

%------------------------------------------------------------------------------
\section{Introduction}
Blokus-Duo is a two-player game, where players take turns setting
pieces of different shapes in a $14 \times 14$ board. This is a
variant of a class of games called zero sum, {\it i.e.}, the gains or
losses of a player are balanced by the losses or gains of their
opponent. This type of problem is a frequent topic of computer science
research, and is present in games such as Chess, Connect6 and Reversi,
just to name three. In fact, Blokus-Duo is a variant of the original
Blokus, which is the same game but with four players~\cite{blokus}.

A Blokus-Duo game is composed of two players, each with 21 available
pieces of different shapes and sizes which are set in a $14 \times 14$
board. Every piece is composed of smaller blocks, where the number of
blocks in a piece gives its score. A piece can only be played once in
the entire game, and can have any rotation as long as the following
rules are met:
\begin{itemize}[nolistsep]
\item{There must be edge-to-edge contact with a piece of the same
  player (Fig.~\ref{fig:edge-to-edge});}
\item{There must not be side-to-side contact with a piece of the same
  player (Fig.~\ref{fig:side-to-side}).}
\end{itemize}
In the first move, each player must occupy the blocks with coordinates
{\it (a,a)} or {\it (5,5)}. The game ends when a player has no moves
left or all pieces have been played. The winner is the player who has
the highest score. The tournament organizers added another rule: both
players must generate a move in $1s$ or less.

\begin{figure}[t]
  \centering
  \begin{subfigure}[b]{0.2\textwidth}
    \centering
    \includegraphics[width=5.0em]{drawings/edge-edge-contact}
    \caption{Edge-to-Edge Contact (Legal)}
    \label{fig:edge-to-edge}
  \end{subfigure}
  ~ %add desired spacing between images, e. g. ~, \quad, \qquad, \hfill etc. 
  %(or a blank line to force the subfigure onto a new line)
  \begin{subfigure}[b]{0.2\textwidth}
    \centering
    \includegraphics[width=5.0em]{drawings/side-side-contact}
    \caption{Side-to-Side Contact (Illegal)}
    \label{fig:side-to-side}
  \end{subfigure}
  \caption{Rules to play a Piece}
  \label{fig:piece-rules}
\end{figure}

A traditional player implementation chooses its best move by
generating all of its and their opponent's moves, and then performing
a search on the tree using miniMax with alpha-beta
pruning~\cite{sugimoto14, kojima14}. If we were to generate a move for
every scenario we easily see the memory usage sky rocket: in the first
move there are $1512$ available moves ($21 \text{ pieces } \times 8
\text{ rotations } \times 9 \text{ extremities}$); in the second move
there may be, in a worst case scenario, $11520$ moves {\em per node}
($20 \text{ pieces } \times 8 \text{ rotations } \times 9 \text{
  extremities } \times 8 \text{ edges}$); in the third move there may
be, in a worst case scenario, $19152$ moves {\em per node} ($19 \text{
  pieces } \times 8 \text{ rotations } \times 9 \text{ extremities }
\times 14 \text{ edges}$), etc. As such, it is not realistic to
generate a move for every node within the $1s$ constraint, not to
mention memory limits. To circumvent this issue, previous
implementations only go 2 or 3 levels deep, do not generate a move for
every node dependent on the alpha-beta pruning performed, and inspect
the nodes with a higher score first~\cite{papadopoulos12}. Another
possible solution would be to design a massively parallel system to
efficiently generate and search any piece for any
corner~\cite{yoza13}.

We decided to take a different and simpler approach to solving the
memory and timing constraint problems, by interpreting the game as a
simple area occupation game. The winner of the game is the one which
occupies the most blocks on the board. Thus, we focus on building
heuristics which limit the playing area of our opponent with the
smallest memory footprint. The program was runs on an Atlas
DE0-Nano-SoC running Linux~\cite{atlas}.

%------------------------------------------------------------------------------
\section{Our Approach}
\label{sec:design}
In order to limit the opponent's plays, we designed a four stage
approach to punish them. These stages can be succinctly described by
the following heuristics:
\begin{itemize}[nolistsep]
\item{Mean distance to all available opponent's edges;}
\item{Blocking one or more available opponent's edges;}
\item{Smallest opponent score;}
\item{Blocking largest empty area.}
\end{itemize}
All moves generated calculate all heuristics and accumulate their
results into a single scoring variable. The move selected will be the
one which has the highest score.

As a first approach we want to limit the opponent's options to remove
free area which is readily available to us. Therefore, the first
heuristic calculates the mean distance between all available
opponent's edges and the center coordinates of the piece to play. Even
though, this does not stop the opponent from playing a large scoring
piece it will, at least, limit its options. This heuristic does not
have a large weight in the final move scoring, but is a good indicator
of the player influence in a given area.

The second step checks whether any block of the piece blocks any
available edge of the opponent. This is a better indication of a
successful move, because it directly impacts the opponent's move set,
as shows Figure~\ref{fig:good-edge-block}. However, this heuristic
needs to be weighted against the available area which faces the
blocking edge(s). An erroneous possibility would be to block an edge
which faces the limits of the board.

\begin{figure}[t]
  \centering
  \includegraphics[width=10em]{drawings/good-edge-block}
  \caption{An example of a good edge block by P2}
  \label{fig:good-edge-block}
\end{figure}

The third heuristic checks which opponent's available pieces can be
played with the highest score. Only the highest scoring pieces are
checked, because they are the ones which can propel the opponent out
of the confined space which we are trying to set up.

The final and most important heuristic is the one which checks whether
the possible move is blocking a large open area
(Fig.~\ref{fig:area-block}). As our entire strategy revolves around
limiting the opponent's space, this speculation carries the most
weight.

Combining all four stages of area control, results in a player which
is extremely aggressive, and will try to limit the opponent's options
as early in the game as possible. Using this strategy, the player will
be at an automatic deficit when it is the second to
start. Furthermore, as the game goes on and the opponent keeps evading
the area containment, our player will degrade in quality.

%------------------------------------------------------------------------------
\section{Conclusion and Future Work}
\label{sec:conc}
In this paper we propose four heuristics, which aim to control the
opponent's playing area by manipulating its playing options. This
software is very simple to implement, and, above all else, has a very
small memory footprint ($15kB$) and can generate and select a move
well below the provided time constraint of $1s$. Since this is a very
specific approach to playing Blokus-Duo, we find this strategy to be
easily countered when expected. However, against a naive
implementation of a standard miniMax alpha-beta pruning approach, it
should win in most cases.

As for future work, we intend to design a more patient player which
tries to be more defensive, looking for opportunities in the
opponent's forced/non-forced errors to gain advantages. Improving the
move generation is also a priority. In most cases the player generates
illegal, or redundant moves. Furthermore, and since the proposed
implementation uses very little memory, this player will be
ported to a smaller Arduino device.

%------------------------------------------------------------------------------

\begin{figure}[t]
  \centering
  \includegraphics[width=15em]{drawings/area-block}
  \caption{An example of an area block by P1}
  \label{fig:area-block}
\end{figure}

\bibliographystyle{IEEEtran}
\bibliography{BIBFile}
%\nocite{*}

\end{document}

%%  LocalWords:  synthesizable
