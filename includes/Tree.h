// Tree implementation
#ifndef TREE_H
#define TREE_H

#include "Board.h"
#include "Players.h"
#include "misc.h"

typedef struct Node {
  int_32 score;
  char piece;
  char rotation;
  char x;
  char y;
} Node;

Node *get_best_move(Bool first_play, Players *player, Board *board,
                    char init_pos, Players *opponent);
#endif
