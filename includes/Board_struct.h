#ifndef BOARD_STRUCT_H
#define BOARD_STRUCT_H
#include "Cells.h"

typedef struct Board {
  Cells cells[14][14];
} Board;

#endif
