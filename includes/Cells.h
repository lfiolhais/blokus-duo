#ifndef CELLS_H
#define CELLS_H
#include "misc.h"

typedef struct Cells {
  Bool has_piece;
  Player player;
} Cells;

// Functions
void print_cell(Cells *self);
void set_piece(Cells *self, Bool value);
Bool has_piece(Cells *self);
void set_cell_player(Cells *self, Player player);
Player get_player_piece(Cells *self);
#endif
