#ifndef REF_FSM_H
#define REF_FSM_H

#include "Players.h"

typedef enum states {
  START,
  FIRST,
  FIRST_US,
  FIRST_THEM,
  US,
  THEM,
  SKIP,
  END
} Ref_States;

void get_next_state(char *op_play, char *param, Ref_States *state,
                    Ref_States *state_nxt, Bool no_more_plays, Players **player,
                    Players **opponent, Players *player_one,
                    Players *player_two, Bool *first_play);

#endif
