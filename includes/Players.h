#ifndef PLAYERS_H
#define PLAYERS_H

#include "Board_struct.h"
#include "misc.h"

typedef struct Playbook {
  uint_16 size;
  uint_16 count;
  uint_8 **data;
} Playbook;

typedef struct Players {
  uint_8 score;
  uint_32 pieces;
  Playbook *playbook;
  Player player;
} Players;

Players *new_player(Player player_id);
uint_16 playbook_len(Playbook *self);
void print_player(Players *self);
void playbook_add(Playbook *self, uint_8 x, uint_8 y);
uint_8 *playbook_get(Playbook *self, uint_8 index);
void playbook_delete(Playbook *self, uint_16 index);
void player_update_score(Players *self, char piece);
void player_update_edges(Players *self, Players *opponent, char piece_char,
                         char rotate_code, char x_coord, char y_coord,
                         uint_8 edge_contact_coord[8][2],
                         uint_8 n_contact_edges_detected, Board *board);
void player_update_pieces(Players *player, char piece_char);
uint_32 player_get_n_pieces(Players *self);
#endif
