#ifndef PIECE_H
#define PIECE_H
#include "misc.h"

// The possible edges of a piece are built of the same components as the piece,
// but to form an edge certain parameters need to be added. For example to form
// the edge 2SW: x (rows) = S + SW and y (columns) = SW; to form the edge 2S3W:
// x (rows) = S + SW and y (columns) = W + SW (in this case the W parameter has
// the value two).
typedef struct PossibleEdges {
  uint_8 W;
  uint_8 E;
  uint_8 S;
  uint_8 N;
} PossibleEdges;

// A piece is represented as
// |    |    | 2N  |    |    |
// |    | NW |  N  | NE |    |
// | 2W |  W |(x,y)|  E | 2E |
// |    | SW |  S  | SE |    |
// |    |    | 2S  |    |    |
// where the center are the x and y coordinates. The score parameter has the
// score for a particular piece. Each parameter represents the number of bloks
// to occupied from the (x,y) center.
typedef struct Piece {
  uint_8 N;
  uint_8 S;
  uint_8 W;
  uint_8 E;
  uint_8 SW;
  uint_8 SE;
  uint_8 NW;
  uint_8 NE;
  uint_8 score;
  uint_8 n_possible_edges;
  PossibleEdges pe[8];
} Piece;

Piece get_piece(char piece, char rotation);
void print_piece(Piece self);
uint_8 get_piece_score(char piece);
uint_8 get_piece_n_edges(char piece);
const PossibleEdges *get_piece_edges(char piece);
PossibleEdges rotate_piece_edges(PossibleEdges self, char rotation_code);
#endif
