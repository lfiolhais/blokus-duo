#ifndef MISC_H
#define MISC_H

#include <stdint.h>

#define DEBUG

//  Types
typedef enum Bool { TRUE, FALSE } Bool;
typedef enum Player { PlayerOne, PlayerTwo } Player;

typedef unsigned char uint_8;
typedef unsigned short uint_16;
typedef int16_t int_16;
typedef uint32_t uint_32;
typedef int32_t int_32;

// Functions
void printerr(const char *restrict format, ...);
uint_8 char_2_int(char char_2_convert);
char int_2_char(uint_8 int_2_convert);
#endif
