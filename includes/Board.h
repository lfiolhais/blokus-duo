#ifndef BOARD_H
#define BOARD_H

#include "Cells.h"
#include "Pieces.h"
#include "Players.h"
#include "Board_struct.h"

typedef enum ValidateMoveTag {
  ValidateMoveTag_Ok,
  ValidateMoveTag_FirstMoveError,
  ValidateMoveTag_BoundariesError,
  ValidateMoveTag_OverlapError,
  ValidateMoveTag_EdgeContactError,
  ValidateMoveTag_UsedPieceError,
  ValidateMoveTag_SideContactError,
} ValidateMoveTag;

typedef struct ValidateMoveResult {
  ValidateMoveTag tag;
  uint_8 n_contact_edges_detected;
  uint_8 edge_contact_coord[8][2];
  union {
    uint_8 FirstMoveError[2];
    uint_8 BoundariesError[2];
    uint_8 OverlapError[2];
    uint_8 EdgeContactError[2];
  } ValidateMoveError;
} ValidateMoveResult;

// Functions
void print_board(Board *self);
Board *new_board(void);
void play_piece(Board *self, char piece, char x_char, char y_char,
                Player player, char rotation);
ValidateMoveResult validate_move(Board *self, Bool first_play, Players *player,
                                 char piece_char, char x_char, char y_char,
                                 char rotation);
#endif
