CC=gcc
# CC=/home/lfiolhais/Projects/fpga-zynq/rocket-chip/riscv-tools/bin/riscv64-unknown-elf-gcc
# CC = /opt/Xilinx/SDK/2017.4/gnu/aarch32/lin/gcc-arm-linux-gnueabi/bin/arm-linux-gnueabihf-gcc
CC_FLAGS=-Wall -Wextra -pedantic -I$(INCDIR) -Werror -std=c99 # -mcpu=cortex-a9 -mfpu=vfpv3 -mfloat-abi=hard #-lm

INCDIR=includes
OUTPUT=blokus
OBJDIR=obj

_DEPS=Cells.h Board.h misc.h Pieces.h Players.h Board_struct.h Tree.h ref_fsm.h
DEPS=$(patsubst %,$(INCDIR)/%,$(_DEPS))

_OBJ= src/main.o src/misc.o src/Board.o src/Cells.o src/Pieces.o src/Players.o src/Tree.o src/ref_fsm.o
OBJ=$(patsubst src/%,$(OBJDIR)/src/%,$(_OBJ))

$(OBJDIR)/%.o: %.c $(DEPS)
	$(CC) $(CC_FLAGS) -c -o $@ $<

all: $(OBJ)
	$(CC) $(CC_FLAGS) $^ -o $(OUTPUT)

debug: CC_FLAGS+=-g
debug: all

release: CC_FLAGS+=-O2
release: all

clean:
	rm -f $(OBJDIR)/src/*.o *~ core $(INCDIR)/*~
	rm -f blokus

.PHONY: clean debug all
